package com.yuriy.connectivityscheduler.view;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.yuriy.connectivityscheduler.ConnectivityService;
import com.yuriy.connectivityscheduler.R;
import com.yuriy.connectivityscheduler.vo.ScheduleParametersVO;
import com.yuriy.connectivityscheduler.SharedPreferencesManager;
import com.yuriy.connectivityscheduler.vo.Status;

import java.lang.ref.WeakReference;

/**
 * {@link com.yuriy.connectivityscheduler.view.MainActivity} is a main view of the application.
 */
public class MainActivity extends ActionBarActivity {

    /**
     * String tag used for the logging messages.
     */
    private static final String CLASS_NAME = MainActivity.class.getSimpleName();

    /**
     * Stores an instance of {@link DownloadHandler}.
     */
    private DownloadHandler mDownloadHandler = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set view
        setContentView(R.layout.activity_main);

        // Initialize the downloadHandler.
        mDownloadHandler = new DownloadHandler(this);

        // Process Wi-Fi check box
        final CheckBox wifiCheckView
                = (CheckBox) findViewById(R.id.manage_wi_fi_check_view);
        wifiCheckView.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        processWiFiCheckSelect(isChecked);
                    }
                }
        );
        processWiFiCheckSelect(SharedPreferencesManager.getManageWiFi(this));

        // Process Bluetooth check box
        final CheckBox bluetoothCheckView
                = (CheckBox) findViewById(R.id.manage_bluetooth_check_view);

        // If Bluetooth adapter is null, which means that there is no Bluetooth on the device,
        // then disable it.
        if (BluetoothAdapter.getDefaultAdapter() == null) {
            bluetoothCheckView.setEnabled(false);
            processBluetoothCheckSelect(false);
        } else {
            bluetoothCheckView.setOnCheckedChangeListener(
                    new CompoundButton.OnCheckedChangeListener() {

                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            processBluetoothCheckSelect(isChecked);
                        }
                    }
            );
            processBluetoothCheckSelect(SharedPreferencesManager.getManageBluetooth(this));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Get status of the tasks running in the service
        startService(ConnectivityService.makeGetStatusIntent(this, mDownloadHandler));

        // Restore state of the activity
        restoreState();
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Save state of the activity
        saveState();

        // Stop service if no task are active
        if (!mDownloadHandler.mIsAnyTasksRunning) {
            stopService(ConnectivityService.makeStopIntent(this));
        }
    }

    @Override
    protected void onDestroy() {

        // Stop service if no task are active
        if (!mDownloadHandler.mIsAnyTasksRunning) {
            stopService(ConnectivityService.makeStopIntent(this));
        }

        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            // DialogFragment.show() will take care of adding the fragment
            // in a transaction.  We also want to remove any currently showing
            // dialog, so make our own transaction and take care of that here.
            final FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                    .beginTransaction();
            final Fragment fragmentByTag = getSupportFragmentManager()
                    .findFragmentByTag(AboutDialog.DIALOG_TAG);
            if (fragmentByTag != null) {
                fragmentTransaction.remove(fragmentByTag);
            }
            fragmentTransaction.addToBackStack(null);

            // Show About Dialog
            final DialogFragment aboutDialog = AboutDialog.newInstance();
            aboutDialog.show(fragmentTransaction, AboutDialog.DIALOG_TAG);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Click handler for the Start - Update Button.
     *
     * @param view {@link android.view.View} associated with the Button.
     */
    public void onStartUpdateServiceClickHandler(final View view) {
        // Make appropriate intent
        final Intent intent = ConnectivityService.makeStartOrUpdateIntent(
                this, createScheduleParameters()
        );

        // Start the ConnectivityService.
        startService(intent);

        // Get status of the tasks running in the service
        startService(ConnectivityService.makeGetStatusIntent(this, mDownloadHandler));
    }

    /**
     * Click handler for the Stop Button.
     *
     * @param view {@link android.view.View} associated with the Button.
     */
    public void onStopServiceClickHandler(final View view) {
        // Stop service
        stopService(ConnectivityService.makeStopIntent(this));

        // Update Start - Stop Button label
        setStartUpdateButtonLabel(
                getString(R.string.start_button_label)
        );

        // Update service status label
        updateServiceStatusLabel(
                getString(R.string.service_not_running_label)
        );
    }

    /**
     * Process Wi-Fi check box selection.
     *
     * @param isChecked True if check box is checked, False - otherwise.
     */
    private void processWiFiCheckSelect(final boolean isChecked) {
        final EditText wifiDurationOn
                = (EditText) findViewById(R.id.wi_fi_duration_on_edit_view);
        final EditText wifiDurationOff
                = (EditText) findViewById(R.id.wi_fi_duration_off_edit_view);
        final TextView wifiDurationOnLabel
                = (TextView) findViewById(R.id.wi_fi_duration_on_label_view);
        final TextView wifiDurationOffLabel
                = (TextView) findViewById(R.id.wi_fi_duration_off_label_view);

        wifiDurationOn.setEnabled(isChecked);
        wifiDurationOff.setEnabled(isChecked);
        wifiDurationOnLabel.setEnabled(isChecked);
        wifiDurationOffLabel.setEnabled(isChecked);
    }

    /**
     * Process Bluetooth check box selection.
     *
     * @param isChecked True if check box is checked, False - otherwise.
     */
    private void processBluetoothCheckSelect(final boolean isChecked) {
        final EditText bluetoothDurationOn
                = (EditText) findViewById(R.id.bluetooth_duration_on_edit_view);
        final EditText bluetoothDurationOff
                = (EditText) findViewById(R.id.bluetooth_duration_off_edit_view);
        final TextView bluetoothDurationOnLabel
                = (TextView) findViewById(R.id.bluetooth_duration_on_label_view);
        final TextView bluetoothDurationOffLabel
                = (TextView) findViewById(R.id.bluetooth_duration_off_label_view);

        bluetoothDurationOn.setEnabled(isChecked);
        bluetoothDurationOff.setEnabled(isChecked);
        bluetoothDurationOnLabel.setEnabled(isChecked);
        bluetoothDurationOffLabel.setEnabled(isChecked);
    }

    /**
     * Create {@link com.yuriy.connectivityscheduler.vo.ScheduleParametersVO} instance with
     * appropriate values for the service.
     *
     * @return Instance of the {@link com.yuriy.connectivityscheduler.vo.ScheduleParametersVO}
     */
    private ScheduleParametersVO createScheduleParameters() {
        final CheckBox wifiCheckView
                = (CheckBox) findViewById(R.id.manage_wi_fi_check_view);
        final CheckBox bluetoothCheckView
                = (CheckBox) findViewById(R.id.manage_bluetooth_check_view);
        final EditText wifiDurationOn
                = (EditText) findViewById(R.id.wi_fi_duration_on_edit_view);
        final EditText wifiDurationOff
                = (EditText) findViewById(R.id.wi_fi_duration_off_edit_view);
        final EditText bluetoothDurationOn
                = (EditText) findViewById(R.id.bluetooth_duration_on_edit_view);
        final EditText bluetoothDurationOff
                = (EditText) findViewById(R.id.bluetooth_duration_off_edit_view);

        final ScheduleParametersVO scheduleParameters = ScheduleParametersVO.makeDefaultInstance();
        scheduleParameters.setIsManageWiFi(wifiCheckView.isChecked());
        scheduleParameters.setIsManageBluetooth(bluetoothCheckView.isChecked());
        scheduleParameters.setWiFiDurationOn(Long.valueOf(wifiDurationOn.getText().toString()));
        scheduleParameters.setWiFiDurationOff(Long.valueOf(wifiDurationOff.getText().toString()));
        scheduleParameters.setBluetoothDurationOn(
                Long.valueOf(bluetoothDurationOn.getText().toString())
        );
        scheduleParameters.setBluetoothDurationOff(
                Long.valueOf(bluetoothDurationOff.getText().toString())
        );

        return scheduleParameters;
    }

    /**
     * @return True is any transport selected to be processed, False - otherwise.
     */
    protected boolean isAnyConnectivitySelected() {
        final CheckBox wifiCheckView
                = (CheckBox) findViewById(R.id.manage_wi_fi_check_view);
        final CheckBox bluetoothCheckView
                = (CheckBox) findViewById(R.id.manage_bluetooth_check_view);

        return wifiCheckView.isChecked() || bluetoothCheckView.isChecked();
    }

    /**
     * Restore state of the activity
     */
    private void restoreState() {
        final CheckBox wifiCheckView
                = (CheckBox) findViewById(R.id.manage_wi_fi_check_view);
        final CheckBox bluetoothCheckView
                = (CheckBox) findViewById(R.id.manage_bluetooth_check_view);
        final EditText wifiDurationOn
                = (EditText) findViewById(R.id.wi_fi_duration_on_edit_view);
        final EditText wifiDurationOff
                = (EditText) findViewById(R.id.wi_fi_duration_off_edit_view);
        final EditText bluetoothDurationOn
                = (EditText) findViewById(R.id.bluetooth_duration_on_edit_view);
        final EditText bluetoothDurationOff
                = (EditText) findViewById(R.id.bluetooth_duration_off_edit_view);

        wifiCheckView.setChecked(SharedPreferencesManager.getManageWiFi(this));
        bluetoothCheckView.setChecked(SharedPreferencesManager.getManageBluetooth(this));
        wifiDurationOn.setText(String.valueOf(SharedPreferencesManager.getWiFiOnTime(this)));
        wifiDurationOff.setText(String.valueOf(SharedPreferencesManager.getWiFiOffTime(this)));
        bluetoothDurationOn.setText(String.valueOf(
                        SharedPreferencesManager.getBluetoothOnTime(this))
        );
        bluetoothDurationOff.setText(String.valueOf(
                        SharedPreferencesManager.getBluetoothOffTime(this))
        );
    }

    /**
     * Save state of the activity
     */
    private void saveState() {
        final CheckBox wifiCheckView
                = (CheckBox) findViewById(R.id.manage_wi_fi_check_view);
        final CheckBox bluetoothCheckView
                = (CheckBox) findViewById(R.id.manage_bluetooth_check_view);
        final EditText wifiDurationOn
                = (EditText) findViewById(R.id.wi_fi_duration_on_edit_view);
        final EditText wifiDurationOff
                = (EditText) findViewById(R.id.wi_fi_duration_off_edit_view);
        final EditText bluetoothDurationOn
                = (EditText) findViewById(R.id.bluetooth_duration_on_edit_view);
        final EditText bluetoothDurationOff
                = (EditText) findViewById(R.id.bluetooth_duration_off_edit_view);

        SharedPreferencesManager.setManageWiFi(this, wifiCheckView.isChecked());
        SharedPreferencesManager.setManageBluetooth(this, bluetoothCheckView.isChecked());
        SharedPreferencesManager.setWiFiOnTime(
                this, Long.valueOf(wifiDurationOn.getText().toString())
        );
        SharedPreferencesManager.setWiFiOffTime(
                this, Long.valueOf(wifiDurationOff.getText().toString())
        );
        SharedPreferencesManager.setBluetoothOnTime(
                this, Long.valueOf(bluetoothDurationOn.getText().toString())
        );
        SharedPreferencesManager.setBluetoothOffTime(
                this, Long.valueOf(bluetoothDurationOff.getText().toString())
        );
    }

    /**
     * Set label for the Start - Update Button.
     *
     * @param label Button label.
     */
    private void setStartUpdateButtonLabel(final String label) {
        final Button button = (Button) findViewById(R.id.start_update_service_btn_view);
        button.setText(label);
    }

    /**
     * Update status of the service.
     *
     * @param label Service status label.
     */
    private void updateServiceStatusLabel(final String label) {
        final TextView textView = (TextView) findViewById(R.id.service_status_view);
        textView.setText(label);
    }

    /**
     * Update Wi-Fi tasks status.
     *
     * @param label Wi-Fi task status label.
     */
    private void updateWiFiStatusLabel(final String label) {
        final TextView textView = (TextView) findViewById(R.id.wi_fi_status_view);
        textView.setText(label);
    }

    /**
     * Update Bluetooth tasks status.
     *
     * @param label Bluetooth task status label.
     */
    private void updateBluetoothStatusLabel(final String label) {
        final TextView textView = (TextView) findViewById(R.id.bluetooth_status_view);
        textView.setText(label);
    }

    /**
     * An inner class that inherits from {@link android.os.Handler}
     * and uses its {@link android.os.Handler#handleMessage(android.os.Message)}
     * hook method to process Messages
     * sent to it from the {@link com.yuriy.connectivityscheduler.ConnectivityService}.
     */
    private static class DownloadHandler extends Handler {

        /**
         * Allows Activity to be garbage collected properly.
         */
        private WeakReference<MainActivity> mActivity;

        /**
         * Boolean field that indicates whether any task is running.
         */
        private boolean mIsAnyTasksRunning = false;

        /**
         * Class constructor constructs {@link #mActivity} as weak reference
         * to the activity.
         *
         * @param activity The corresponding activity.
         */
        public DownloadHandler(final MainActivity activity) {
            mActivity = new WeakReference<MainActivity>(activity);
        }

        /**
         * This hook method is dispatched in response to receiving the
         * pathname back from the {@link com.yuriy.connectivityscheduler.ConnectivityService}.
         */
        public void handleMessage(final Message message) {

            final MainActivity activity = mActivity.get();

            // Fail out if the MainActivity is gone.
            if (activity == null) {
                return;
            }

            // Get message Id
            final int what = message.what;

            // Process message from the service
            switch (what) {

                case ConnectivityService.ServiceHandler.MSG_GET_STATUS:

                    mIsAnyTasksRunning = ConnectivityService.extractIsAnyTasksRunning(message);

                    if (mIsAnyTasksRunning) {
                        activity.setStartUpdateButtonLabel(
                                activity.getString(R.string.update_button_label)
                        );
                        activity.updateServiceStatusLabel(
                                activity.getString(R.string.service_running_label)
                        );
                    } else {
                        activity.setStartUpdateButtonLabel(
                                activity.getString(R.string.start_button_label)
                        );
                        activity.updateServiceStatusLabel(
                                activity.getString(R.string.service_not_running_label)
                        );
                    }

                    break;

                case ConnectivityService.ServiceHandler.MSG_WI_FI_ON:

                    final String valueOn
                            = String.format(
                            activity.getString(R.string.wi_fi_on_status_label),
                            ConnectivityService.extractWiFiOnDuration(message)
                    );

                    activity.updateWiFiStatusLabel(valueOn);
                    break;

                case ConnectivityService.ServiceHandler.MSG_WI_FI_OFF:

                    final String valueOff
                            = String.format(
                            activity.getString(R.string.wi_fi_off_status_label),
                            ConnectivityService.extractWiFiOffDuration(message)
                    );

                    activity.updateWiFiStatusLabel(valueOff);
                    break;

                case ConnectivityService.ServiceHandler.MSG_BLUETOOTH_ON:

                    final String valueBluetoothOn
                            = String.format(
                            activity.getString(R.string.bluetooth_on_status_label),
                            ConnectivityService.extractBluetoothOnDuration(message)
                    );

                    activity.updateBluetoothStatusLabel(valueBluetoothOn);
                    break;

                case ConnectivityService.ServiceHandler.MSG_BLUETOOTH_OFF:

                    final String valueBluetoothOff
                            = String.format(
                            activity.getString(R.string.bluetooth_off_status_label),
                            ConnectivityService.extractBluetoothOffDuration(message)
                    );

                    activity.updateBluetoothStatusLabel(valueBluetoothOff);
                    break;


                default:
                    Log.d(CLASS_NAME, "Unknown message id received:" + what);
            }
        }
    }
}
