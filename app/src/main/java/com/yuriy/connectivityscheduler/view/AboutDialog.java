/*
 * Copyright 2014 The "Open Radio" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuriy.connectivityscheduler.view;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.yuriy.connectivityscheduler.R;
import com.yuriy.connectivityscheduler.utils.AppUtils;
import com.yuriy.connectivityscheduler.utils.IntentsHelper;

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 12/20/14
 * E-Mail: chernyshov.yuriy@gmail.com
 */

/**
 * {@link com.yuriy.connectivityscheduler.view.AboutDialog} is view which contains information
 * about this application.
 */
public class AboutDialog extends DialogFragment {

    /**
     * Tag string to use in logging message.
     */
    private static final String CLASS_NAME = AboutDialog.class.getSimpleName();

    /**
     * Tag string to use in dialog transactions.
     */
    public static final String DIALOG_TAG = CLASS_NAME + "_DIALOG_TAG";

    /**
     * My profile's url
     */
    private static final String AUTHOR_PROFILE_URL
            = "http://www.linkedin.com/pub/yuriy-chernyshov/1b/622/270";

    /**
     * Project's url
     */
    private static final String PROJECT_HOME_URL
            = "https://bitbucket.org/ChernyshovYuriy/connectivityscheduler/overview";

    /**
     * Create a new instance of {@link com.yuriy.openradio.view.AboutDialog}
     */
    @SuppressWarnings("all")
    public static AboutDialog newInstance() {
        final AboutDialog aboutDialog = new AboutDialog();
        // provide here an arguments, if any
        return aboutDialog;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {

        getDialog().setTitle(getActivity().getString(R.string.app_name) + "    v." +
                AppUtils.getApplicationVersion(getActivity()));

        final View view = inflater.inflate(R.layout.dialog_about, container, false);

        final TextView authorLink = (TextView) view.findViewById(R.id.about_author_link_view);
        authorLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(IntentsHelper.makeUrlBrowsableIntent(AUTHOR_PROFILE_URL));
            }
        });

        final TextView projectHomeLink = (TextView) view.findViewById(R.id.about_project_link_view);
        projectHomeLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(IntentsHelper.makeUrlBrowsableIntent(PROJECT_HOME_URL));
            }
        });

        final Button closeBtn = (Button) view.findViewById(R.id.about_dialog_close_btn_view);
        closeBtn.setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        getDialog().dismiss();
                    }
                }
        );

        return view;
    }
}
