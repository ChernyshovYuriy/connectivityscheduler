package com.yuriy.connectivityscheduler;

import android.app.Activity;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.yuriy.connectivityscheduler.vo.ScheduleParametersVO;
import com.yuriy.connectivityscheduler.vo.Status;

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 12/27/14
 * E-Mail: chernyshov.yuriy@gmail.com
 */

/**
 * {@link com.yuriy.connectivityscheduler.ConnectivityService} is a main service of the application.
 * It manage Wi-Fi and Bluetooth connectivity.
 */
public class ConnectivityService extends Service {

    /**
     * String tag used for the logging messages.
     */
    private static final String CLASS_NAME = ConnectivityService.class.getSimpleName();

    /**
     * Key which is used for Command passing to the service, it contains a Name of the Command.
     */
    private static final String KEY_COMMAND_NAME = "KEY_COMMAND_NAME";

    /**
     * Key which is used for "Start or Update" Command.
     */
    private static final String COMMAND_START_OR_UPDATE_PARAMETERS
            = "COMMAND_START_OR_UPDATE_PARAMETERS";

    /**
     * Key which is used for "Get Status" Command.
     */
    private static final String COMMAND_GET_STATUS= "COMMAND_GET_STATUS";

    /**
     * Key which is used for Parameters which are passing into the service.
     */
    private static final String KEY_SCHEDULE_PARAMETERS = "KEY_SCHEDULE_PARAMETERS";

    /**
     * Key which is used for Messenger which is passing into the service.
     */
    private static final String KEY_MESSENGER = "KEY_MESSENGER";

    /**
     * Key for the {@link android.os.Bundle} store to hold a
     * {@link com.yuriy.connectivityscheduler.vo.Status}.
     */
    private static final String BUNDLE_KEY_STATUS = "BUNDLE_KEY_STATUS";

    /**
     * Handler to use as a timer for the Wi-Fi to be switched On.
     */
    private final Handler wifiOnHandler = new Handler();

    /**
     * Handler to use as a timer for the Wi-Fi to be switched Off.
     */
    private final Handler wifiOffHandler = new Handler();

    /**
     * Duration of the Wi-Fi On state, in seconds
     */
    private long mWiFiDurationOn;

    /**
     * Duration of the Wi-Fi Off state, in seconds
     */
    private long mWiFiDurationOff;

    /**
     * Handler to use as a timer for the Bluetooth to be switched On.
     */
    private final Handler bluetoothOnHandler = new Handler();

    /**
     * Handler to use as a timer for the Bluetooth to be switched Off.
     */
    private final Handler bluetoothOffHandler = new Handler();

    /**
     * Duration of the Bluetooth On state, in seconds
     */
    private long mBluetoothDurationOn;

    /**
     * Duration of the Bluetooth Off state, in seconds
     */
    private long mBluetoothDurationOff;

    /**
     * Processes Messages sent to it from onStartCommnand() that
     * indicate what command to process.
     */
    private volatile ServiceHandler mServiceHandler;

    /**
     * Looper associated with the HandlerThread.
     */
    private volatile Looper mServiceLooper;

    /**
     * Indicated whether Wi-Fi task is running.
     */
    private volatile boolean mIsWiFiTaskRunning = false;

    /**
     * Indicated whether Bluetooth task is running.
     */
    private volatile boolean mIsBluetoothTaskRunning = false;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i(CLASS_NAME, "Service Created");

        // Create and start a background HandlerThread since by
        // default a Service runs in the UI Thread, which we don't
        // want to block.
        final HandlerThread thread = new HandlerThread("ConnectivityServiceThread");
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler.
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    public int onStartCommand(final Intent intent, final int flags, final int startId) {

        if (BuildConfig.DEBUG) {
            Log.i(CLASS_NAME, "On Start Command, intent:" + intent + ", flags:" + flags
                    + ", startId:" + startId);
        }

        // Various bound conditions checks

        if (intent == null) {
            return super.onStartCommand(intent, flags, startId);
        }

        if (!intent.hasExtra(KEY_COMMAND_NAME)) {
            return super.onStartCommand(intent, flags, startId);
        }

        final String command = intent.getStringExtra(KEY_COMMAND_NAME);

        if (command == null) {
            return super.onStartCommand(intent, flags, startId);
        }

        Log.i(CLASS_NAME, "Service Command:" + command);

        // Process command

        if (command.equals(COMMAND_START_OR_UPDATE_PARAMETERS)) {
            startOrUpdateTasks(intent);
        } else if (command.equals(COMMAND_GET_STATUS)) {
            // Create a Message that will be sent to ServiceHandler to
            // retrieve a data based on the command name in the Intent.
            final Message message = mServiceHandler.makeGetStatusMessage(intent);

            // Send the Message to ServiceHandler to retrieve tasks status.
            mServiceHandler.sendMessage(message);
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {

        // Stop all tasks when destroying
        stopAllTasks();

        // Quit looper
        mServiceLooper.quit();

        super.onDestroy();

        Log.i(CLASS_NAME, "Service Destroyed");
    }

    /**
     * Factory method to make {@link android.content.Intent} which will start or update current
     * tasks.
     *
     * @param context            Context of the callee.
     * @param scheduleParameters Tasks parameters.
     */
    public static Intent makeStartOrUpdateIntent(final Context context,
                                                 final ScheduleParametersVO scheduleParameters) {
        // Create the Intent that's associated to the ConnectivityService class.
        final Intent intent = new Intent(context, ConnectivityService.class);

        // Set command name
        intent.putExtra(KEY_COMMAND_NAME, COMMAND_START_OR_UPDATE_PARAMETERS);

        // Set the Parameters of the scheduling in the Intent.
        intent.putExtra(KEY_SCHEDULE_PARAMETERS, scheduleParameters);

        return intent;
    }

    /**
     * Factory method to make {@link android.content.Intent} which will start "get status" method
     * of the service.
     *
     * @param context Context of the callee.
     * @param handler Handler to replay result over.
     */
    public static Intent makeGetStatusIntent(final Context context,
                                             final Handler handler) {
        // Create the Intent that's associated to the ConnectivityService class.
        final Intent intent = new Intent(context, ConnectivityService.class);

        // Set command name
        intent.putExtra(KEY_COMMAND_NAME, COMMAND_GET_STATUS);

        // Set the Messenger object which will use for the replay message.
        intent.putExtra(KEY_MESSENGER, new Messenger(handler));

        return intent;
    }

    /**
     * Factory method to make the desired {@link android.content.Intent}.
     *
     * @param context Context of the callee.
     */
    public static Intent makeStopIntent(final Context context) {
        // Create the Intent that's associated to the ConnectivityService class.
        return new Intent(context, ConnectivityService.class);
    }

    /**
     * Helper method to inform whether any task is running.
     *
     * @param message {@link android.os.Message} returned from the service to activity.
     * @return True in case of there is any task running, False - otherwise.
     */
    public static boolean extractIsAnyTasksRunning(final Message message) {
        if (message.getData() == null) {
            return false;
        }
        final Bundle bundle = message.getData();
        final Status status = (Status) bundle.getSerializable(BUNDLE_KEY_STATUS);
        return status != null && status != Status.NOT_RUNNING;
    }

    /**
     * Helper method to extract Wi-Fi ON duration from the provided message.
     *
     * @param message {@link android.os.Message} returned from the service to activity.
     * @return Wi-Fi ON duration, in seconds
     */
    public static long extractWiFiOnDuration(final Message message) {
        if (message.obj == null) {
            return SharedPreferencesManager.DEFAULT_PERIOD;
        }
        return (long) message.obj;
    }

    /**
     * Helper method to extract Wi-Fi OFF duration from the provided message.
     *
     * @param message {@link android.os.Message} returned from the service to activity.
     * @return Wi-Fi OFF duration, in seconds
     */
    public static long extractWiFiOffDuration(final Message message) {
        if (message.obj == null) {
            return SharedPreferencesManager.DEFAULT_PERIOD;
        }
        return (long) message.obj;
    }

    /**
     * Helper method to extract Bluetooth ON duration from the provided message.
     *
     * @param message {@link android.os.Message} returned from the service to activity.
     * @return Wi-Fi ON duration, in seconds
     */
    public static long extractBluetoothOnDuration(final Message message) {
        if (message.obj == null) {
            return SharedPreferencesManager.DEFAULT_PERIOD;
        }
        return (long) message.obj;
    }

    /**
     * Helper method to extract Bluetooth OFF duration from the provided message.
     *
     * @param message {@link android.os.Message} returned from the service to activity.
     * @return Wi-Fi OFF duration, in seconds
     */
    public static long extractBluetoothOffDuration(final Message message) {
        if (message.obj == null) {
            return SharedPreferencesManager.DEFAULT_PERIOD;
        }
        return (long) message.obj;
    }

    /**
     * Runnable for the Wi-Fi On task.
     */
    private final Runnable wifiOnRunnable = new Runnable() {

        @Override
        public void run() {

            processWiFiOnHandler();
        }
    };

    /**
     * Runnable for the Wi-Fi Off task.
     */
    private final Runnable wifiOffRunnable = new Runnable() {

        @Override
        public void run() {

            processWiFiOffHandler();
        }
    };

    /**
     * Switch Wi-Fi Off.
     */
    private void wifiSwitchOff() {
        final WifiManager wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(false);

        if (BuildConfig.DEBUG) {
            Log.d(CLASS_NAME, "Wi-Fi OFF");
        }

        mServiceHandler.sendWiFiOff();
    }

    /**
     * Switch Wi-Fi On.
     */
    private void wifiSwitchOn() {
        final WifiManager wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(true);

        if (BuildConfig.DEBUG) {
            Log.d(CLASS_NAME, "Wi-Fi ON");
        }

        mServiceHandler.sendWiFiOn();
    }

    /**
     * Runnable for the Bluetooth On task.
     */
    private final Runnable bluetoothOnRunnable = new Runnable() {

        @Override
        public void run() {

            processBluetoothOnHandler();
        }
    };

    /**
     * Runnable for the Bluetooth Off task.
     */
    private final Runnable bluetoothOffRunnable = new Runnable() {

        @Override
        public void run() {

            processBluetoothOffHandler();
        }
    };

    /**
     * Switch Bluetooth Off.
     */
    private void bluetoothSwitchOff() {
        final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothAdapter.disable();

        if (BuildConfig.DEBUG) {
            Log.d(CLASS_NAME, "Bluetooth OFF");
        }

        mServiceHandler.sendBluetoothOff();
    }

    /**
     * Switch Bluetooth On.
     */
    private void bluetoothSwitchOn() {
        final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothAdapter.enable();

        if (BuildConfig.DEBUG) {
            Log.d(CLASS_NAME, "Bluetooth ON");
        }

        mServiceHandler.sendBluetoothOn();
    }

    /**
     * Stop all tasks.
     */
    private void stopAllTasks() {
        mIsWiFiTaskRunning = false;
        mIsBluetoothTaskRunning = false;
        wifiOnHandler.removeCallbacks(wifiOnRunnable);
        wifiOffHandler.removeCallbacks(wifiOffRunnable);
        bluetoothOnHandler.removeCallbacks(bluetoothOnRunnable);
        bluetoothOffHandler.removeCallbacks(bluetoothOffRunnable);
    }

    /**
     * Start or Update all tasks.
     *
     * @param intent {@link android.content.Intent} passed from the Activity.
     */
    private void startOrUpdateTasks(final Intent intent) {
        // Extract parameters.
        final ScheduleParametersVO scheduleParameters
                = (ScheduleParametersVO) intent.getSerializableExtra(KEY_SCHEDULE_PARAMETERS);

        if (scheduleParameters == null) {
            return;
        }

        if (BuildConfig.DEBUG) {
            Log.d(CLASS_NAME, "Add or update " + scheduleParameters);
        }

        // Stop tasks if necessary
        stopAllTasks();

        // Schedule Wi-Fi is needed
        if (scheduleParameters.isManageWiFi()) {
            mWiFiDurationOn = scheduleParameters.getWiFiDurationOn();
            mWiFiDurationOff = scheduleParameters.getWiFiDurationOff();

            processWiFiOffHandler();
        }

        // Schedule Bluetooth is needed
        if (scheduleParameters.isManageBluetooth()) {
            mBluetoothDurationOn = scheduleParameters.getBluetoothDurationOn();
            mBluetoothDurationOff = scheduleParameters.getBluetoothDurationOff();

            processBluetoothOffHandler();
        }
    }

    /**
     * Method to process Wi-Fi On handler.
     */
    private void processWiFiOnHandler() {
        wifiSwitchOff();
        mIsWiFiTaskRunning = true;
        wifiOffHandler.postDelayed(wifiOffRunnable, mWiFiDurationOff * 1000);
    }

    /**
     * Method to process Wi-Fi Off handler.
     */
    private void processWiFiOffHandler() {
        wifiSwitchOn();
        mIsWiFiTaskRunning = true;
        wifiOnHandler.postDelayed(wifiOnRunnable, mWiFiDurationOn * 1000);
    }

    /**
     * Method to process Bluetooth On handler.
     */
    private void processBluetoothOnHandler() {
        bluetoothSwitchOff();
        mIsBluetoothTaskRunning = true;
        bluetoothOffHandler.postDelayed(bluetoothOffRunnable, mBluetoothDurationOff * 1000);
    }

    /**
     * Method to process Bluetooth Off handler.
     */
    private void processBluetoothOffHandler() {
        bluetoothSwitchOn();
        mIsBluetoothTaskRunning = true;
        bluetoothOnHandler.postDelayed(bluetoothOnRunnable, mBluetoothDurationOn * 1000);
    }

    /**
     * @return actual {@link com.yuriy.connectivityscheduler.vo.Status} of the service.
     */
    private Status getStatus() {
        if (mIsWiFiTaskRunning && !mIsBluetoothTaskRunning) {
            return Status.WI_FI_RUNNING;
        }
        if (!mIsWiFiTaskRunning && mIsBluetoothTaskRunning) {
            return Status.BLUETOOTH_RUNNING;
        }
        if (mIsWiFiTaskRunning && mIsBluetoothTaskRunning) {
            return Status.WI_FI_AND_BLUETOOTH_RUNNING;
        }
        return Status.NOT_RUNNING;
    }

    /**
     * An inner class that inherits from {@link android.os.Handler} and uses its
     * {@link #handleMessage(android.os.Message)} hook method to process Messages sent to
     * it from {@link #onStartCommand(android.content.Intent, int, int)} that indicate which
     * data to respond.
     */
    public final class ServiceHandler extends Handler {

        /**
         * Make message with the status of the service.
         */
        public static final int MSG_GET_STATUS =   1;

        /**
         * Make message which indicates that Wi-Fi is turned ON.
         */
        public static final int MSG_WI_FI_ON =     2;

        /**
         * Make message which indicates that Wi-Fi is turned OFF.
         */
        public static final int MSG_WI_FI_OFF =    3;

        /**
         * Make message which indicates that Bluetooth is turned ON.
         */
        public static final int MSG_BLUETOOTH_ON = 4;

        /**
         * Make message which indicates that Bluetooth is turned OFF.
         */
        public static final int MSG_BLUETOOTH_OFF = 5;

        /**
         * Messenger to use to replay to the Activity.
         */
        private Messenger mMessenger = null;

        /**
         * Class constructor initializes the Looper.
         *
         * @param looper The Looper that we borrow from HandlerThread.
         */
        public ServiceHandler(final Looper looper) {
            super(looper);
        }

        /**
         * Hook method that retrieves an image from a remote server.
         */
        @Override
        public void handleMessage(final Message message) {
            // Extract message
            final int what = message.what;

            final Intent intent = (Intent) message.obj;

            if (intent == null) {
                Log.w(CLASS_NAME, "ServiceHandler handle null intent");
                return;
            }

            // Extract the Messenger.
            mMessenger = (Messenger) intent.getExtras().get(KEY_MESSENGER);

            if (mMessenger == null) {
                Log.w(CLASS_NAME, "ServiceHandler handle null messenger");
                return;
            }

            switch (what) {
                case MSG_GET_STATUS:
                    if (BuildConfig.DEBUG) {
                        Log.d(CLASS_NAME, "ServiceHandler handle Get Status");
                    }
                    sendStatus(mMessenger);
                    break;
                default:
                    Log.w(CLASS_NAME, "ServiceHandler handle unknown message:" + what);
            }
        }

        /**
         * Send the status back to the MainActivity via the Messenger.
         *
         * @param messenger {@link android.os.Messenger}
         */
        private void sendStatus(final Messenger messenger) {
            // Call factory method to create Message.
            final Message message = makeReplyMessageWithStatus();

            try {
                // Send status to back to the MainActivity.
                message.what = MSG_GET_STATUS;
                messenger.send(message);
            } catch (RemoteException e) {
                Log.e(CLASS_NAME, "Exception while sending:" + e.getMessage());
            }
        }

        /**
         * Send message about Wi-Fi On event.
         */
        private void sendWiFiOn() {
            if (mMessenger == null) {
                Log.w(CLASS_NAME, "Can not send Wi-Fi On state, messenger is null");
                return;
            }

            // Call factory method to create Message.
            final Message message = makeEmptyReplyMessage();

            try {
                // Send Wi-Fi On status to the MainActivity.
                message.what = MSG_WI_FI_ON;
                // Set duration ON to the message's field
                message.obj = mWiFiDurationOn;
                mMessenger.send(message);
            } catch (RemoteException e) {
                Log.e(CLASS_NAME, "Exception while sending:" + e.getMessage());
            }
        }

        /**
         * Send message about Wi-Fi Off event.
         */
        private void sendWiFiOff() {
            if (mMessenger == null) {
                Log.w(CLASS_NAME, "Can not send Wi-Fi Off state, messenger is null");
                return;
            }

            // Call factory method to create Message.
            final Message message = makeEmptyReplyMessage();

            try {
                // Send status to back to the MainActivity.
                message.what = MSG_WI_FI_OFF;
                // Set duration OFF to the message's field
                message.obj = mWiFiDurationOff;
                mMessenger.send(message);
            } catch (RemoteException e) {
                Log.e(CLASS_NAME, "Exception while sending:" + e.getMessage());
            }
        }

        /**
         * Send message about Bluetooth On event.
         */
        private void sendBluetoothOn() {
            if (mMessenger == null) {
                Log.w(CLASS_NAME, "Can not send Bluetooth On state, messenger is null");
                return;
            }

            // Call factory method to create Message.
            final Message message = makeEmptyReplyMessage();

            try {
                // Send Bluetooth On status to the MainActivity.
                message.what = MSG_BLUETOOTH_ON;
                // Set duration ON to the message's field
                message.obj = mBluetoothDurationOn;
                mMessenger.send(message);
            } catch (RemoteException e) {
                Log.e(CLASS_NAME, "Exception while sending:" + e.getMessage());
            }
        }

        /**
         * Send message about Bluetooth Off event.
         */
        private void sendBluetoothOff() {
            if (mMessenger == null) {
                Log.w(CLASS_NAME, "Can not send Bluetooth Off state, messenger is null");
                return;
            }

            // Call factory method to create Message.
            final Message message = makeEmptyReplyMessage();

            try {
                // Send Bluetooth Off status to the MainActivity.
                message.what = MSG_BLUETOOTH_OFF;
                // Set duration OFF to the message's field
                message.obj = mBluetoothDurationOff;
                mMessenger.send(message);
            } catch (RemoteException e) {
                Log.e(CLASS_NAME, "Exception while sending:" + e.getMessage());
            }
        }

        /**
         * A factory method that creates a {@link android.os.Message} that contains
         * information on the tasks status.
         */
        private Message makeGetStatusMessage(final Intent intent) {

            final Message message = Message.obtain();
            // Include Intent in Message to indicate which command to process.
            message.obj = intent;
            message.what = MSG_GET_STATUS;
            return message;
        }

        /**
         * A factory method that creates a Message to return to the
         * {@link com.yuriy.connectivityscheduler.view.MainActivity} with the status of the tasks.
         *
         */
        private Message makeReplyMessageWithStatus() {
            final Message message = Message.obtain();

            // Return the result to indicate whether the response succeeded or failed.
            message.arg1 = Activity.RESULT_OK;

            final Bundle data = new Bundle();

            // Data of the downloaded offer.
            data.putSerializable(BUNDLE_KEY_STATUS, getStatus());
            message.setData(data);
            return message;
        }

        /**
         * A factory method that creates an empty Message to return to the
         * {@link com.yuriy.connectivityscheduler.view.MainActivity}.
         *
         */
        private Message makeEmptyReplyMessage() {
            final Message message = Message.obtain();

            // Return the result to indicate whether the response succeeded or failed.
            message.arg1 = Activity.RESULT_OK;
            return message;
        }
    }
}
