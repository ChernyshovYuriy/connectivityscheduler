package com.yuriy.connectivityscheduler;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 12/27/14
 * E-Mail: chernyshov.yuriy@gmail.com
 */

/**
 * {@link com.yuriy.connectivityscheduler.SharedPreferencesManager} is a manager over
 * Shared Preferences of the application.
 */
public class SharedPreferencesManager {

    /**
     * Preferences file name.
     */
    private static final String PREFS_NAME = "ConnectivitySchedulerPreferences";

    /**
     * Key for the "Enable Wi-Fi" connectivity manage.
     */
    private static final String PREF_KEY_MANAGE_WI_FI = "PREF_KEY_MANAGE_WI_FI";

    /**
     * Key for the "Enable Bluetooth" connectivity manage.
     */
    private static final String PREF_KEY_MANAGE_BLUETOOTH = "PREF_KEY_MANAGE_BLUETOOTH";

    /**
     * Key for the "Wi-Fi On Time" value.
     */
    private static final String PREF_KEY_WI_FI_ON_TIME = "PREF_KEY_WI_FI_ON_TIME";

    /**
     * Key for the "Wi-Fi Off Time" value.
     */
    private static final String PREF_KEY_WI_FI_OFF_TIME = "PREF_KEY_WI_FI_OFF_TIME";

    /**
     * Key for the "Bluetooth On Time" value.
     */
    private static final String PREF_KEY_BLUETOOTH_ON_TIME = "PREF_KEY_BLUETOOTH_ON_TIME";

    /**
     * Key for the "Bluetooth Off Time" value.
     */
    private static final String PREF_KEY_BLUETOOTH_OFF_TIME = "PREF_KEY_BLUETOOTH_OFF_TIME";

    /**
     * Default period for the On and Off states, in seconds.
     */
    public static final long DEFAULT_PERIOD = 1;

    /**
     * @param context Context of the callee.
     *
     * @return {@link android.content.SharedPreferences} of the Application
     */
    private static SharedPreferences getSharedPreferences(final Context context) {
        return context.getSharedPreferences(PREFS_NAME, 0);
    }

    /**
     * @param context Context of the callee.
     *
     * @return {@link android.content.SharedPreferences.Editor}
     */
    private static SharedPreferences.Editor getEditor(final Context context) {
        return getSharedPreferences(context).edit();
    }

    /**
     * Set True if it is necessary to schedule Wi-Fi.
     *
     * @param context Context of the callee.
     * @param value   True or False.
     */
    public static void setManageWiFi(final Context context, final boolean value) {
        final SharedPreferences.Editor editor = getEditor(context);
        editor.putBoolean(PREF_KEY_MANAGE_WI_FI, value);
        editor.apply();
    }

    /**
     * @param context Context of the callee.
     *
     * @return True if it is necessary to schedule Wi-Fi.
     */
    public static boolean getManageWiFi(final Context context) {
        return getSharedPreferences(context).getBoolean(PREF_KEY_MANAGE_WI_FI, false);
    }

    /**
     * Set True if it is necessary to schedule Bluetooth.
     *
     * @param context Context of the callee.
     * @param value   True or False.
     */
    public static void setManageBluetooth(final Context context, final boolean value) {
        final SharedPreferences.Editor editor = getEditor(context);
        editor.putBoolean(PREF_KEY_MANAGE_BLUETOOTH, value);
        editor.apply();
    }

    /**
     * @param context Context of the callee.
     *
     * @return True if it is necessary to schedule Bluetooth.
     */
    public static boolean getManageBluetooth(final Context context) {
        return getSharedPreferences(context).getBoolean(PREF_KEY_MANAGE_BLUETOOTH, false);
    }

    /**
     * Set duration of the "ON" state for the Wi-Fi.
     *
     * @param context Context of the callee.
     * @param value   Duration of the "ON" state.
     */
    public static void setWiFiOnTime(final Context context, final long value) {
        final SharedPreferences.Editor editor = getEditor(context);
        editor.putLong(PREF_KEY_WI_FI_ON_TIME, value);
        editor.apply();
    }

    /**
     * @param context Context of the callee.
     *
     * @return Duration of the "ON" state for the Wi-Fi.
     */
    public static long getWiFiOnTime(final Context context) {
        return getSharedPreferences(context).getLong(PREF_KEY_WI_FI_ON_TIME, DEFAULT_PERIOD);
    }

    /**
     * Set duration of the "OFF" state for the Wi-Fi.
     *
     * @param context Context of the callee.
     * @param value   Duration of the "OFF" state.
     */
    public static void setWiFiOffTime(final Context context, final long value) {
        final SharedPreferences.Editor editor = getEditor(context);
        editor.putLong(PREF_KEY_WI_FI_OFF_TIME, value);
        editor.apply();
    }

    /**
     * @param context Context of the callee.
     *
     * @return Duration of the "OFF" state for the Wi-Fi.
     */
    public static long getWiFiOffTime(final Context context) {
        return getSharedPreferences(context).getLong(PREF_KEY_WI_FI_OFF_TIME, DEFAULT_PERIOD);
    }

    /**
     * Set duration of the "ON" state for the Bluetooth.
     *
     * @param context Context of the callee.
     * @param value   Duration of the "ON" state.
     */
    public static void setBluetoothOnTime(final Context context, final long value) {
        final SharedPreferences.Editor editor = getEditor(context);
        editor.putLong(PREF_KEY_BLUETOOTH_ON_TIME, value);
        editor.apply();
    }

    /**
     * @param context Context of the callee.
     *
     * @return Duration of the "ON" state for the Bluetooth.
     */
    public static long getBluetoothOnTime(final Context context) {
        return getSharedPreferences(context).getLong(PREF_KEY_BLUETOOTH_ON_TIME, DEFAULT_PERIOD);
    }

    /**
     * Set duration of the "OFF" state for the Bluetooth.
     *
     * @param context Context of the callee.
     * @param value   Duration of the "OFF" state.
     */
    public static void setBluetoothOffTime(final Context context, final long value) {
        final SharedPreferences.Editor editor = getEditor(context);
        editor.putLong(PREF_KEY_BLUETOOTH_OFF_TIME, value);
        editor.apply();
    }

    /**
     * @param context Context of the callee.
     *
     * @return Duration of the "OFF" state for the Bluetooth.
     */
    public static long getBluetoothOffTime(final Context context) {
        return getSharedPreferences(context).getLong(PREF_KEY_BLUETOOTH_OFF_TIME, DEFAULT_PERIOD);
    }
}
