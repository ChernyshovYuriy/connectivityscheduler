package com.yuriy.connectivityscheduler.vo;

import java.io.Serializable;

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 12/28/14
 * E-Mail: chernyshov.yuriy@gmail.com
 */

/**
 * {@link Status} is an enumeration of the service statuses.
 */
public enum Status implements Serializable {

    /**
     * None of the tasks are running
     */
    NOT_RUNNING,

    /**
     * Only Wi-Fi tasks is running
     */
    WI_FI_RUNNING,

    /**
     * Only Bluetooth tasks is running
     */
    BLUETOOTH_RUNNING,

    /**
     * Wi-Fi and Bluetooth tasks are running
     */
    WI_FI_AND_BLUETOOTH_RUNNING
}
