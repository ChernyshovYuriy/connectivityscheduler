package com.yuriy.connectivityscheduler.vo;

import java.io.Serializable;

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 12/27/14
 * E-Mail: chernyshov.yuriy@gmail.com
 */

/**
 * {@link ScheduleParametersVO} is a Value Object class
 * which holds information about parameters for the service start or update.
 */
public class ScheduleParametersVO implements Serializable {

    private boolean mIsManageWiFi;

    private boolean mIsManageBluetooth;

    private long mWiFiDurationOn;

    private long mWiFiDurationOff;

    private long mBluetoothDurationOn;

    private long mBluetoothDurationOff;

    /**
     * Private constructor.
     */
    private ScheduleParametersVO() {}

    public boolean isManageWiFi() {
        return mIsManageWiFi;
    }

    public void setIsManageWiFi(boolean value) {
        mIsManageWiFi = value;
    }

    public boolean isManageBluetooth() {
        return mIsManageBluetooth;
    }

    public void setIsManageBluetooth(boolean value) {
        mIsManageBluetooth = value;
    }

    public long getWiFiDurationOn() {
        return mWiFiDurationOn;
    }

    public void setWiFiDurationOn(long value) {
        mWiFiDurationOn = value;
    }

    public long getWiFiDurationOff() {
        return mWiFiDurationOff;
    }

    public void setWiFiDurationOff(long value) {
        mWiFiDurationOff = value;
    }

    public long getBluetoothDurationOn() {
        return mBluetoothDurationOn;
    }

    public void setBluetoothDurationOn(long value) {
        mBluetoothDurationOn = value;
    }

    public long getBluetoothDurationOff() {
        return mBluetoothDurationOff;
    }

    public void setBluetoothDurationOff(long value) {
        mBluetoothDurationOff = value;
    }

    public static ScheduleParametersVO makeDefaultInstance() {
        return new ScheduleParametersVO();
    }

    @Override
    public String toString() {
        return "ScheduleParametersVO{" +
                "isManageWiFi=" + mIsManageWiFi +
                ", isManageBluetooth=" + mIsManageBluetooth +
                ", wiFiDurationOn=" + mWiFiDurationOn +
                ", wiFiDurationOff=" + mWiFiDurationOff +
                ", bluetoothDurationOn=" + mBluetoothDurationOn +
                ", bluetoothDurationOff=" + mBluetoothDurationOff +
                '}';
    }
}
