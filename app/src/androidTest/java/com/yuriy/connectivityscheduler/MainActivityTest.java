package com.yuriy.connectivityscheduler;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.CheckBox;

import com.yuriy.connectivityscheduler.view.MainActivity;

import java.util.concurrent.CountDownLatch;

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 12/28/14
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    public MainActivityTest() {
        super(MainActivity.class);
    }

    public void testConnectivitySelectedReturnTrueIfAnySelected() throws InterruptedException {
        final CheckBox wifiCheckView
                = (CheckBox) getActivity().findViewById(R.id.manage_wi_fi_check_view);
        final CheckBox bluetoothCheckView
                = (CheckBox) getActivity().findViewById(R.id.manage_bluetooth_check_view);

        final CountDownLatch latch_A = new CountDownLatch(1);

        getActivity().runOnUiThread(
                new Runnable() {

                    @Override
                    public void run() {
                        wifiCheckView.setChecked(true);
                        bluetoothCheckView.setChecked(false);

                        latch_A.countDown();
                    }
                }
        );

        latch_A.await();

        assertTrue(getActivity().isAnyConnectivitySelected());

        final CountDownLatch latch_B = new CountDownLatch(1);

        getActivity().runOnUiThread(
                new Runnable() {

                    @Override
                    public void run() {
                        wifiCheckView.setChecked(false);
                        bluetoothCheckView.setChecked(true);

                        latch_B.countDown();
                    }
                }
        );

        latch_B.await();

        assertTrue(getActivity().isAnyConnectivitySelected());
    }

    public void testConnectivitySelectedReturnTrueIfAllSelected() throws InterruptedException {
        final CheckBox wifiCheckView
                = (CheckBox) getActivity().findViewById(R.id.manage_wi_fi_check_view);
        final CheckBox bluetoothCheckView
                = (CheckBox) getActivity().findViewById(R.id.manage_bluetooth_check_view);

        final CountDownLatch latch = new CountDownLatch(1);

        getActivity().runOnUiThread(
                new Runnable() {

                    @Override
                    public void run() {
                        wifiCheckView.setChecked(true);
                        bluetoothCheckView.setChecked(true);

                        latch.countDown();
                    }
                }
        );

        latch.await();

        assertTrue(getActivity().isAnyConnectivitySelected());
    }

    public void testConnectivitySelectedReturnFalseIfNoneSelected() throws InterruptedException {
        final CheckBox wifiCheckView
                = (CheckBox) getActivity().findViewById(R.id.manage_wi_fi_check_view);
        final CheckBox bluetoothCheckView
                = (CheckBox) getActivity().findViewById(R.id.manage_bluetooth_check_view);

        final CountDownLatch latch = new CountDownLatch(1);

        getActivity().runOnUiThread(
                new Runnable() {

                    @Override
                    public void run() {
                        wifiCheckView.setChecked(false);
                        bluetoothCheckView.setChecked(false);

                        latch.countDown();
                    }
                }
        );

        latch.await();

        assertFalse(getActivity().isAnyConnectivitySelected());
    }
}
